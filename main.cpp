#include <iostream>
//#include "csv.h"
#include "rapidcsv.h"
#include <typeinfo>
#include <string>
#include <iterator>
#include <chrono>
#include<fstream>
#include<thread>
#include<list>
#include<algorithm>

using namespace std;
using namespace std::chrono;

//Read file
rapidcsv::Document doc("song_df.csv", rapidcsv::LabelParams(-1, -1));

std::vector<string> id = doc.GetColumn<string>(0);
std::vector<string> song_name = doc.GetColumn<string>(1);
std::vector<string> song_start = doc.GetColumn<string>(2);
std::vector<string> lr = doc.GetColumn<string>(3);
std::vector<string> lp = doc.GetColumn<string>(4);
std::vector<string> mr = doc.GetColumn<string>(5);
std::vector<string> mp = doc.GetColumn<string>(6);
std::vector<string> cp = doc.GetColumn<string>(7);

int df_length = song_name.size();

// create array
string *to_arr(vector<string> x_vec);

string *id_arr = to_arr(id);
string *song_name_arr = to_arr(song_name);
string *song_start_arr = to_arr(song_start);
string *lr_arr = to_arr(lr);
string *lp_arr = to_arr(lp);
string *mr_arr = to_arr(mr);
string *mp_arr = to_arr(mp);
string *cp_arr = to_arr(cp);


ofstream song_status_output1("song_status1.csv");
ofstream song_status_output2("song_status2.csv");
ofstream song_status_output3("song_status3.csv");
ofstream song_status_output4("song_status4.csv");
ofstream song_status_output5("song_status5.csv");
ofstream song_status_output6("song_status6.csv");
ofstream song_status_output7("song_status7.csv");
ofstream song_status_output8("song_status8.csv");
ofstream song_status_output9("song_status9.csv");
ofstream song_status_output10("song_status10.csv");
ofstream song_status_output11("song_status11.csv");
ofstream song_status_output12("song_status12.csv");

int name_compare(int a, int b);
int lyric_compare(int a , int b);
int author_compare(int a, int b);
int company_compare(int a, int b);

void song_status_func(int i, ofstream &file_input);
void song_status_func_thread(int loop_start, int loop_end, ofstream &file_input);

//---------------------------------------- main--------------------------------
int main(int argc, char *argv[])
{
    cout << argc << " parameters passed:" << endl;
    for(int i{0}; i <argc; i++){
        cout << argv[i] << endl;
    }

    int  interval_1= stoi(argv[1]);
    int  interval_2= stoi(argv[2]);
    int  interval_3= stoi(argv[3]);
    int  interval_4= stoi(argv[4]);
    int  interval_5= stoi(argv[5]);
    int  interval_6= stoi(argv[6]);
    int  interval_7= stoi(argv[7]);
    int  interval_8= stoi(argv[8]);
    int  interval_9= stoi(argv[9]);
    int  interval_10= stoi(argv[10]);
    int  interval_11= stoi(argv[11]);
    int  interval_12= stoi(argv[12]);
    int  interval_13= stoi(argv[13]);

//    int interval_1 = 1;
//    int interval_2 = 12;
//    int interval_3 = 24;
//    int interval_4 = 36;
//    int interval_5 = 48;
//    int interval_6 = 60;
//    int interval_7 = 72;
//    int interval_8 = 84;
//    int interval_9 = 96;
//    int interval_10 = 108;
//    int interval_11 = 120;
//    int interval_12 = 132;
//    int interval_13 = 144;


    cout << "df_length equal: " << df_length + 1 << endl;

    song_status_output1 << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                        ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output2  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output3  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output4  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output5 << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                        ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output6  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";;
    song_status_output7  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output8  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output9  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                         ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output10  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                          ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output11  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                          ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";
    song_status_output12  << "original_id" <<", "  << "compare_song_id" << ", " << "result" <<
                          ","<< "name_status" << " ," << "lyric_status" << ", " << "author_status" << ", " << "company_status" "\n";


    /* thread split*/

//song_status_func1(1, song_status_output1);
    thread song_compute1(song_status_func_thread,interval_1,interval_2, ref(song_status_output1));
    thread song_compute2(song_status_func_thread,interval_2,interval_3, ref(song_status_output2));
    thread song_compute3(song_status_func_thread,interval_3,interval_4, ref(song_status_output3));
    thread song_compute4(song_status_func_thread,interval_4,interval_5, ref(song_status_output4));
    thread song_compute5(song_status_func_thread,interval_5,interval_6, ref(song_status_output5));
    thread song_compute6(song_status_func_thread,interval_6,interval_7, ref(song_status_output6));
    thread song_compute7(song_status_func_thread,interval_7,interval_8, ref(song_status_output7));
    thread song_compute8(song_status_func_thread,interval_8,interval_9, ref(song_status_output8));
    thread song_compute9(song_status_func_thread,interval_9,interval_10, ref(song_status_output9));
    thread song_compute10(song_status_func_thread,interval_10,interval_11, ref(song_status_output10));
    thread song_compute11(song_status_func_thread,interval_11,interval_12, ref(song_status_output11));
    thread song_compute12(song_status_func_thread,interval_12,interval_13 +1  , ref(song_status_output12));

    song_compute1.join();
    song_compute2.join();
    song_compute3.join();
    song_compute4.join();
    song_compute5.join();
    song_compute6.join();
    song_compute7.join();
    song_compute8.join();
    song_compute9.join();
    song_compute10.join();
    song_compute11.join();
    song_compute12.join();

    return 0;

}// end of main
//--------------------------------------------------------------------------------

//----------------------function used--------

// convert vector to array
string *to_arr(vector<string> x_vec){

    int vector_count = x_vec.size();
    cout << "Vector Count: " << vector_count << endl;
    string *x_arr = new string[vector_count];
    for (int i = 0; i < vector_count; i++){
        x_arr[i] = x_vec[i];}
    cout << "x_arr: " << typeid(x_arr).name() << " Address: " << x_arr << endl;
    cout << endl;
    std::vector<string>().swap(x_vec);
    return x_arr;

}
//#####################################################################################################################
// song status function1
void song_status_func(int i, ofstream &file_input){

    // 0 = unknown, 1 = ไม่ซ้ำซ้อน, 2 = ซ้ำซ้อน, 3=เข้าข่าย
//    cout << "song status" << endl;
    int result;

//    vector<string> result_output_vector;
    for(int j = 1; j < df_length; j++){

        int company_status = company_compare(i,j);
        //same company continue
        if(company_status == 1){
            continue;
        }

        //condition 21-32 ************************************
        else if((song_name_arr[i] != song_name_arr[j]) &&
           ((song_start_arr[i] != song_start_arr[j]) ||
            (song_start_arr[i] == "no_data" || song_start_arr[j] == "no_data"))
                ){
            continue;

            //condition 8******************************
        }    else if(
                (song_name_arr[i] == song_name_arr[j]) &&
                        ((song_start_arr[i] != song_start_arr[j]) && ((song_start_arr[i] != "no_data")&&(song_start_arr[j] != "no_data")) ) &&
                (lr_arr[i] == "no_data" &&
                 lr_arr[j] == "no_data" &&
                 lp_arr[i] == "no_data" &&
                 lp_arr[j] == "no_data" &&
                 mr_arr[i] == "no_data" &&
                 mr_arr[j] == "no_data" &&
                 mp_arr[i] == "no_data" &&
                 mp_arr[j] == "no_data" )


                ) {
            continue;

            // condition 6+ 10 + 14 ************************************
        }else if(
                (song_name_arr[i] == song_name_arr[j])&&

                (
                        ((song_start_arr[i] != song_start_arr[j]) &&
                         (song_start_arr[i] != "no_data" && song_start_arr[j] != "no_data")
                        ) ||
                        ((song_start_arr[i] == "no_data")||(song_start_arr[j] == "no_data"))

                )&&

                //song master not the same
                (
                        (
                                (lr_arr[i] != lr_arr[j]) &&
                                (lp_arr[i] != lp_arr[j]) &&
                                (mr_arr[i] != mr_arr[j]) &&
                                (mp_arr[i] != mp_arr[j])
                        ) &&
                        ((lr_arr[i] !="no_data") && (lr_arr[j] != "no_data") &&
                         (lp_arr[i] != "no_data") && ( lp_arr[j] != "no_data") &&
                         (mr_arr[i] != "no_data" ) && ( mr_arr[j] != "no_data") &&
                         (mp_arr[i] != "no_data") && ( mp_arr[j] != "no_data"))

                )


                ){
            continue;

            //condition 13+16 ************************************
        }else if(
                (song_name_arr[i] == song_name_arr[j]) &&
                ((song_start_arr[i] == "no_data")&&(song_start_arr[j] == "no_data")) &&

                (
                        //con con 13
                        ((
                                ((lr_arr[i] == lr_arr[j])&&(lr_arr[i] != "no_data")) ||
                                ((lp_arr[i] == lp_arr[j])&&(lp_arr[i] != "no_data")) ||
                                ((mr_arr[i] == mr_arr[j])&&(mr_arr[i] != "no_data")) ||
                                ((mp_arr[i] == mp_arr[j])&&(mp_arr[i] != "no_data"))

                                )
//                                 &&
//                         ((lr_arr[i] !="no_data") && (lr_arr[j] != "no_data") &&
//                          (lp_arr[i] != "no_data") &&( lp_arr[j] != "no_data") &&
//                          (mr_arr[i] != "no_data" ) &&( mr_arr[j] != "no_data") &&
//                          (mp_arr[i] != "no_data") && ( mp_arr[j] != "no_data"))

                        ) ||
                        //con 16
                        (lr_arr[i] == "no_data" &&
                         lr_arr[j] == "no_data" &&
                         lp_arr[i] == "no_data" &&
                         lp_arr[j] == "no_data" &&
                         mr_arr[i] == "no_data" &&
                         mr_arr[j] == "no_data" &&
                         mp_arr[i] == "no_data" &&
                         mp_arr[j] == "no_data" )
                )

                ) {
            result = 2;

            //condition 1 ************************************
        }else if(
                (song_name_arr[i] == song_name_arr[j]) &&

                (
                        (song_start_arr[i] == song_start_arr[j]) &&
                        (song_start_arr[i] != "no_data" || song_start_arr[j] != "no_data")
                )&&
                ((
                                ((lr_arr[i] == lr_arr[j])&&(lr_arr[i] != "no_data")) ||
                                ((lp_arr[i] == lp_arr[j])&&(lp_arr[i] != "no_data")) ||
                                ((mr_arr[i] == mr_arr[j])&&(mr_arr[i] != "no_data")) ||
                                ((mp_arr[i] == mp_arr[j])&&(mp_arr[i] != "no_data"))
                        )
//                         &&
//                 ((lr_arr[i] !="no_data") && (lr_arr[j] != "no_data") &&
//                  (lp_arr[i] != "no_data") &&( lp_arr[j] != "no_data") &&
//                  (mr_arr[i] != "no_data" ) &&( mr_arr[j] != "no_data") &&
//                  (mp_arr[i] != "no_data") && ( mp_arr[j] != "no_data"))
                )
                ){
            result = 2;

            //all other condition ************************************
        }else{result = 3;}
        int name_status = name_compare(i,j);
        int lyric_status = lyric_compare(i,j);
        int author_status = author_compare(i,j);


        cout << "original song id:  " << id_arr[i] <<
             "; compare song id: " << id_arr[j] << "; status: " << result <<"; name_status: "<< name_status <<
             "; lyric_status: " << lyric_status<< "; author_status: " << author_status <<"; company_status: " << company_status <<  endl;

        file_input << id_arr[i] << ", " << id_arr[j] << ", " << result << ", " << name_status << ", " << lyric_status <<
                   ", " << author_status << ", " << company_status <<"\n";

    }//end of j loop
}// end of song status function

//thread function
void song_status_func_thread(int loop_start, int loop_end, ofstream &file_input){
    auto start = high_resolution_clock::now();

    for (int i = loop_start; i < loop_end; i++) {
        song_status_func(i, file_input);
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);

    cout << "Time taken by function: " << duration.count() << " milliseconds" << endl;
    file_input.close();
}

//compare function*********************************************************

// same name function
int name_compare(int a, int b){
// one song calculation
    int result;
    result = song_name_arr[a] == song_name_arr[b] ? 1 : 2;
    return result;
}


// same start function
int lyric_compare(int a, int b){

    int result;
    if((song_start_arr[a] == "no_data") && (song_start_arr[b] == "no_data")){
        result = 4;
    }else if((song_start_arr[a] == "no_data") || (song_start_arr[b] == "no_data")){
        result = 3;
    }else if(song_start_arr[a] == song_start_arr[b]){
        result = 1;
    }else{result = 2;}

    return result;
}

int author_compare(int a, int b){
// one song calculation
    int result;


    if (
            ((lr_arr[a] == lr_arr[b]) ||
             (lp_arr[a] == lp_arr[b]) ||
             (mr_arr[a] == mr_arr[b]) ||
             (mp_arr[a] == mp_arr[b])) &&
            ((lr_arr[a] !="no_data") && (lr_arr[b] != "no_data") &&
             (lp_arr[a] != "no_data") &&( lp_arr[b] != "no_data") &&
             (mr_arr[a] != "no_data" ) &&( mr_arr[b] != "no_data") &&
             (mp_arr[a] != "no_data") && ( mp_arr[b] != "no_data"))

            ) {
        result = 1;
    } else if(
        // not equal and not a singlu NA
            ((lr_arr[a] != lr_arr[b]) &&
             (lp_arr[a] != lp_arr[b]) &&
             (mr_arr[a] != mr_arr[b]) &&
             (mp_arr[a] != mp_arr[b])) &&
            ((lr_arr[a] !="no_data") && (lr_arr[b] != "no_data") &&
             (lp_arr[a] != "no_data") &&( lp_arr[b] != "no_data") &&
             (mr_arr[a] != "no_data" ) &&( mr_arr[b] != "no_data") &&
             (mp_arr[a] != "no_data") && ( mp_arr[b] != "no_data"))

            ){
        result = 2;
    } else if(
            (lr_arr[a] =="no_data") && (lr_arr[b] == "no_data") &&
            (lp_arr[a] == "no_data") &&( lp_arr[b] == "no_data") &&
            (mr_arr[a] == "no_data" ) &&( mr_arr[b] == "no_data") &&
            (mp_arr[a] == "no_data") && ( mp_arr[b] == "no_data")
            ){
        result = 4;
    }else {result = 3;}

    return result;
}

int company_compare(int a, int b){
// one song calculation
    int result;
    result = cp_arr[a] == cp_arr[b] ? 1 : 2;
    return result;
}
//###########################################################################################




