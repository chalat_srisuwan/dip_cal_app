#%%
import pandas as pd
import pyodbc
import os
import sqlalchemy
from urllib.parse import quote_plus
import sys, getopt
import time
import numpy as np
import glob
import pymysql
import mysql.connector

#%%
pyodbc.drivers()
#%% test
import_table = 'reg_songs'
export_table = 'status_output'
year_id = 1
#%% example string
# python cal_song_py import_table export_table row_include year_id_include
# python cal_song.py reg_songs year2_status all 2
#%%
if len(sys.argv) > 1:
    import_table = sys.argv[1]
    export_table = sys.argv[2]

else:
    import_table = 'reg_songs'
    export_table = 'status_output'
print("\nImport Table: ")
print(import_table)
print("\nExport Table: ")
print(export_table)

#%% year_id
if len(sys.argv) > 1:
    year_id = sys.argv[4]

else:
    year_id = 'all'
#%%
con = pyodbc.connect('DRIVER={MySQL ODBC 8.0 Driver};SERVER=127.0.0.1:3307;DATABASE=CPR_SONG;UID=root;PWD=1w2q3r4e')
con_string = 'DRIVER={MySQL ODBC 8.0 Driver};SERVER=127.0.0.1:3307;DATABASE=CPR_SONG;UID=root;PWD=1w2q3r4e'
cursor = con.cursor()


engine_connector = sqlalchemy.create_engine("mysql+mysqlconnector://root:1w2q3r4e@127.0.0.1:3307/CPR_SONG", echo = False)
#%% create song_df and start_df
if year_id == "all":
    query = "select * from " + import_table

else:
    query = "select * from " + import_table + " where year_id = " + str(year_id)


song_df = pd.read_sql(query, con)
song_df.to_csv('song_df_raw.csv')
#%%
# backup_df = song_df.copy(deep = True)
#%%
# song_df = backup_df.copy(deep = True)
#%%
song_df = song_df[['id','name', 'lyrics','lyric_author_name','lyric_author_pen_name','compose_author_name','compose_author_pen_name']]

#%%
song_df.columns =["id", "song_name", "song_start", "lr","lp", "mr","mp"]

#%%
song_df = song_df.replace(to_replace=',',value = ';',regex = True)
song_df = song_df.replace(to_replace=' ',value = '',regex = True)
song_df = song_df.replace(to_replace='_x000D_\n',value = '',regex = True)
song_df = song_df.replace(to_replace='\n',value = '',regex = True)

#%%
song_df = song_df.fillna(value="no_data")

song_df = song_df.sort_values(by=['id'])
#%%
start_df = song_df.copy(deep=True)

#%%
song_df.to_csv('song_df.csv', index = False)
print('Table Imported.')

#%%
# dat = pd.read_csv('/home/chalat/CLionProjects/song/song_df.csv')
#%% command line with arguments passing to c++
if sys.argv[3] == "all":
    interval_1 = 1
    interval_13 = len(song_df)
    interval_2 = round(interval_13/12)
    interval_3 = round(interval_13/12)*2
    interval_4 = round(interval_13/12)*3
    interval_5 = round(interval_13/12)*4
    interval_6 = round(interval_13/12)*5
    interval_7 = round(interval_13/12)*6
    interval_8 = round(interval_13/12)*7
    interval_9 = round(interval_13/12)*8
    interval_10 = round(interval_13/12)*9
    interval_11 = round(interval_13/12)*10
    interval_12 = round(interval_13/12)*11



else:

    interval_1 = 1
    interval_13 = int(sys.argv[3])
    interval_2 = round(interval_13/12)
    interval_3 = round(interval_13/12)*2
    interval_4 = round(interval_13/12)*3
    interval_5 = round(interval_13/12)*4
    interval_6 = round(interval_13/12)*5
    interval_7 = round(interval_13/12)*6
    interval_8 = round(interval_13/12)*7
    interval_9 = round(interval_13/12)*8
    interval_10 = round(interval_13/12)*9
    interval_11 = round(interval_13/12)*10
    interval_12 = round(interval_13/12)*11
#%%
command_exec = './song ' + \
               str(interval_1) + ' ' + str(interval_2) + ' ' + str(interval_3) + ' ' + str(interval_4) + \
               ' ' + str(interval_5) + ' ' + str(interval_6) + ' ' + str(interval_7) + ' ' + str(interval_8) + \
               ' ' + str(interval_9) + ' ' + str(interval_10) + ' ' + str(interval_11) + ' ' + str(interval_12) +\
               ' ' + str(interval_13)

#%%
os.system(command_exec)
print('Calculation Completed.')


#%%
song_status_list = glob.glob("song_status*.*")
#%%
song_status = pd.concat([pd.read_csv(i, skipinitialspace=True) for i in song_status_list])
#%%
song_status = song_status.sort_values(by=['original_id','compare_song_id'])
#%%make song_id
start_df = start_df.rename(columns = {'id':'original_id'}, errors = "raise")
#%%
song_status = song_status.merge(start_df, on='original_id', how = 'left')
#%%
song_status['song_status_index'] = [x+1 for x in list(song_status.index)]

#%%
song_status.to_csv('full_song_status.csv', index = False)

#%%
song_status = song_status[['song_status_index','original_id', 'compare_song_id', 'result']]
#%%
song_status['year_id'] = year_id

#%%
# os.system(upload_exec)
print("Start Uploading")
start = time.time()
song_status.to_sql(export_table, con = engine_connector, if_exists='replace', index = False)
stop = time.time()
print("Upload Time: ")
print(stop-start)
print("Upload Completed.")

#%% test start here #############################################################
#
# dat = pd.read_csv("/home/chalat/CLionProjects/song/song_df_test.csv")
# 
#
# #%%
# dat.to_sql("test_table", con = engine_connector, if_exists='replace', index = False)

