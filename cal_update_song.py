#%%
import pandas as pd
import pyodbc
import os
import sqlalchemy
from urllib.parse import quote_plus
import sys, getopt
import time
import numpy as np
import glob
import pymysql
import mysql.connector
from sqlalchemy.dialects import mysql
#%% test
import_table = 'reg_songs'
export_table = 'reg_songs_duplication'
year_id = 95
#%%
con = pyodbc.connect('DRIVER={MySQL ODBC 8.0 Driver};SERVER=10.10.18.85:3306;DATABASE=CPR_SONG;UID=dipsong;PWD=1w2q3r4e')
con_string = 'DRIVER={MySQL ODBC 8.0 Driver};SERVER=10.10.18.85:3306;DATABASE=CPR_SONG;UID=dipsong;PWD=1w2q3r4e'
cursor = con.cursor()


engine_connector = sqlalchemy.create_engine("mysql+mysqlconnector://dipsong:1w2q3r4e@10.10.18.85:3306/CPR_SONG", echo = False)
#%% year_id
year_id = sys.argv[4]
#%%check for calculating
reg_years = pd.read_sql("select * from reg_years", con)

#%%
# if sum(reg_years["active"].isin(["Calculating"])) >0 :
#     print("\nThere is another task in progress. ")
#     print(reg_years)
#     print("Terminating request. \n")
#     quit()

#%%calculating status update in reg_years
lock_query = "update reg_years set active = 'Calculating' where id = " + str(year_id)
cursor.execute(lock_query)
con.commit()
#%% example string
# python cal_song_py import_table export_table row_include year_id_include
# python cal_song.py reg_songs year2_status all 2
#%%
if len(sys.argv) > 1:
    import_table = sys.argv[1]
    export_table = sys.argv[2]

else:
    import_table = 'reg_songs'
    export_table = 'status_output'
print("\nImport Table: ")
print(import_table)
print("\nExport Table: ")
print(export_table)

#%% create song_df and start_df

query = "select * from " + import_table + " where year_id = " + str(year_id) + \
            " and is_musical = 1 and song_type = 'thai'"

song_df = pd.read_sql(query, con)
song_df.to_csv('song_df_raw.csv')

#%%
# test = pd.read_sql("select * from reg_songs_test where year_id = 1", con)
#%%
# backup_df = song_df.copy(deep = True)
#%%
# song_df = backup_df.copy(deep = True)
# song_df = pd.read_csv("song_df_raw.csv")
#%%
song_df = song_df[['id','exactly_name', 'exactly_lyrics','lyric_author_name','lyric_author_pen_name','compose_author_name','compose_author_pen_name', 'company_id']]

#%%
song_df.columns =["id", "song_name", "song_start", "lr","lp", "mr","mp", "cp"]

#%%
song_df = song_df.replace(to_replace=',',value = ';',regex = True)
song_df = song_df.replace(to_replace=' ',value = '',regex = True)
song_df = song_df.replace(to_replace='_x000D_\n',value = '',regex = True)
song_df = song_df.replace(to_replace='\n',value = '',regex = True)

#%%
song_df = song_df.fillna(value="no_data")
song_df = song_df.sort_values(by=['id'])
#%%
song_df = song_df.replace(r'^\s*$', "no_data", regex=True)
#%%
start_df = song_df.copy(deep=True)

#%%
song_df.to_csv('song_df.csv', index = False)
print('Table Imported.')

#%%
# dat = pd.read_csv('/home/chalat/CLionProjects/song/song_df.csv')
#%% command line with arguments passing to c++
if sys.argv[3] == "all":
    interval_1 = 1
    interval_13 = len(song_df)
    interval_2 = round(interval_13/12)
    interval_3 = round(interval_13/12)*2
    interval_4 = round(interval_13/12)*3
    interval_5 = round(interval_13/12)*4
    interval_6 = round(interval_13/12)*5
    interval_7 = round(interval_13/12)*6
    interval_8 = round(interval_13/12)*7
    interval_9 = round(interval_13/12)*8
    interval_10 = round(interval_13/12)*9
    interval_11 = round(interval_13/12)*10
    interval_12 = round(interval_13/12)*11

else:

    interval_1 = 1
    interval_13 = int(sys.argv[3])
    interval_2 = round(interval_13/12)
    interval_3 = round(interval_13/12)*2
    interval_4 = round(interval_13/12)*3
    interval_5 = round(interval_13/12)*4
    interval_6 = round(interval_13/12)*5
    interval_7 = round(interval_13/12)*6
    interval_8 = round(interval_13/12)*7
    interval_9 = round(interval_13/12)*8
    interval_10 = round(interval_13/12)*9
    interval_11 = round(interval_13/12)*10
    interval_12 = round(interval_13/12)*11
#%%
command_exec = './song ' + \
               str(interval_1) + ' ' + str(interval_2) + ' ' + str(interval_3) + ' ' + str(interval_4) + \
               ' ' + str(interval_5) + ' ' + str(interval_6) + ' ' + str(interval_7) + ' ' + str(interval_8) + \
               ' ' + str(interval_9) + ' ' + str(interval_10) + ' ' + str(interval_11) + ' ' + str(interval_12) +\
               ' ' + str(interval_13)

#%%
os.system(command_exec)
print('Calculation Completed.')

#%% combine result file
print("current working directory: ")
print(os.getcwd())
song_status_list = glob.glob("song_status*.*")

#%%
# song_status = pd.concat([pd.read_csv(i, skipinitialspace = True) for i in song_status_list])
#%%
try:
    file1 = pd.read_csv("song_status1.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file2 = pd.read_csv("song_status2.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file3 = pd.read_csv("song_status3.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file4 = pd.read_csv("song_status4.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file5 = pd.read_csv("song_status5.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file6 = pd.read_csv("song_status6.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file7 = pd.read_csv("song_status7.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file8 = pd.read_csv("song_status8.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file9 = pd.read_csv("song_status9.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file10 = pd.read_csv("song_status10.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file11 = pd.read_csv("song_status11.csv", skipinitialspace= True)
except:
    print('blank_file\n')

try:
    file12 = pd.read_csv("song_status12.csv", skipinitialspace= True)
except:
    print('blank_file\n')

#%%
column_names = ['original_id','compare_song_id', 'result','name_status','lyric_status','author_status','company_status']
song_status = pd.DataFrame(columns = column_names)
print(song_status)
#%%
try:
    song_status = pd.concat([song_status, file1], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file2], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file3], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file4], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file5], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file6], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file7], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file8], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file9], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file10], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file11], axis = 0)
except:
    print('file not found')

try:
    song_status = pd.concat([song_status, file12], axis = 0)
except:
    print('file not found')

print(song_status)

#%%
song_status = song_status[song_status.original_id != song_status.compare_song_id]
song_status = song_status.sort_values(by=['original_id','compare_song_id'])

#%%make song_id
start_df = start_df.rename(columns = {'id':'original_id'}, errors = "raise")
#%%
song_status = song_status.merge(start_df, on='original_id', how = 'left')
#%%
song_status['song_status_index'] = [x+1 for x in list(song_status.index)]

#%%
# song_status.to_csv('full_song_status.csv', index = False)
#%%
# song_status = pd.read_csv("full_song_status.csv")
#%%
# song_status = song_status[['song_status_index','original_id', 'compare_song_id', 'result']]
#%%
song_status.rename(columns = {'name_status':'duplicate_name',
                              'lyric_status':'duplicate_lyric',
                              'author_status':'duplicate_author',
                              'company_status':'duplicate_company'
                              }, inplace = True)

#%%
# song_status.columns
#%%
song_status['year_id'] = year_id
song_status['duplicate_state'] = song_status['result']
#%%
song_status = song_status[['song_status_index','original_id','compare_song_id','result','duplicate_name','duplicate_lyric',
             'duplicate_author','duplicate_company', 'duplicate_state', 'year_id']]
#%%delete previous record of the same year
delete_query = "DELETE FROM " + export_table + " WHERE year_id = " + str(year_id)
try:
    cursor.execute(delete_query)
    con.commit()
    print("Replacing results in an existing table.\n")
except:
    print("Creating a new result table.\n")
#%%
# os.system(upload_exec)
print("\n\n###Start Uploading: " + str(song_status.shape[0]) + " rows of result###")

start = time.time()
song_status.to_sql(export_table, con = engine_connector, if_exists='append', index = False, chunksize = 100000,
                   dtype={
                        'song_status_index': mysql.MEDIUMINT(unsigned=True),
                       'original_id':mysql.MEDIUMINT(unsigned=True),
                       'compare_song_id': mysql.MEDIUMINT(unsigned=True),
                       'result': mysql.TINYINT(unsigned=True),
                       'duplicate_name': mysql.TINYINT(unsigned=True),
                       'duplicate_lyric': mysql.TINYINT(unsigned=True),
                       'duplicate_author': mysql.TINYINT(unsigned=True),
                       'duplicate_company': mysql.TINYINT(unsigned=True),
                       'duplicate_state': mysql.TINYINT(unsigned=True),
                       'year_id': mysql.TINYINT(unsigned=True)

                          })
stop = time.time()
print("Upload Completed. With " + str(stop-start) + " seconds upload time.")
#%% update evaluation_status
print("\n\n###Updating evaluation_status started.###")

if not start_df.empty:
   update_query = "UPDATE " +import_table + " SET evaluation_status = 2 where id in " + str(tuple(start_df.original_id.unique()))
   cursor.execute(update_query)
   con.commit()
   print("Updating evaluation_status completed.")
   print(cursor.rowcount, "record(s) updated")

#%% update evaluation_result
song_status['result'] = song_status['result'].astype(int)
min_status = song_status.loc[song_status.groupby('original_id')['result'].idxmin()]

#%%#%%     // 0 = unknown, 1 = ไม่ซ้ำซ้อน, 2 = ซ้ำซ้อน, 3=เข้าข่าย
#set initial result for calculated data to 1
print('\n\n###evaluation_result update###')
if not start_df.empty:
  update_query = "UPDATE " + import_table + " SET evaluation_result = 1 where id in " + str(tuple(start_df.original_id.unique()))
  cursor.execute(update_query)
  con.commit()


#%%set evaluation_result of calculated id with result 3 to 3

print("Updating evaluation_result3 started.")
if len(min_status[min_status['result'] == 3]['original_id']) >0:
    a = str(list(min_status[min_status['result'] == 3]['original_id']))
    update_id = "(%s)" % str(a).strip('[]')

    update_query = "UPDATE " + import_table + " SET evaluation_result = 3 where id in " + \
                   update_id

    cursor.execute(update_query)
    con.commit()
    print("Updating evaluation_result3 completed.")
    print(cursor.rowcount, "record(s) updated")
else:print("no result = 3 data")
#%%set evaluation_result of calculated id with result 2 to 2
print("Updating evaluation_result2 started.")
if len(min_status[min_status['result'] == 2]['original_id']) >0:
    a = str(list(min_status[min_status['result'] == 2]['original_id']))
    update_id = "(%s)" % str(a).strip('[]')

    update_query = "UPDATE " +import_table + " SET evaluation_result = 2 where id in " + \
                   update_id


    cursor.execute(update_query)
    con.commit()
    print("Updating evaluation_result2 completed.")
    print(cursor.rowcount, "record(s) updated")
else:print("no result = 2 data")



#%% update evaluation_override
print("\n\n update evaluation_override = result")
try:
    update_query = "UPDATE " +import_table + " SET evaluation_override = evaluation_result where year_id = " + \
                       str(year_id)
    cursor.execute(update_query)
    con.commit()
    print("\n Completed.")
except:
    print('can not update evaluation_override = evaluation_result')
#%% update evaluation_override
try:
    print("\n\n update evaluation_override: replace 3 with 0")
    update_query = "UPDATE " +import_table + " SET evaluation_override = 0 where evaluation_override = 3 and year_id = " + \
                       str(year_id)
    cursor.execute(update_query)
    con.commit()
    print("\n Completed.")
except:
    print('can not update evaluation_override = 0 where evaluation_override = 3')
#########################################################################################################################
#%% update year procedure

try:
    earlier_year_id = \
        pd.read_sql("select id from reg_years where year = (select year-1 from reg_years where id =" + str(year_id) + ");",
                                  con)

    earlier_id = earlier_year_id['id'][0]

    override_query = "call update_override(" + str(year_id) + ", " + str(
        earlier_id) + ")"  ## edit store procedure
    cursor.execute(override_query)
    con.commit()
    print("\nOverride completed.")
except:
    print("Can not override")

#%%remove calculating status in reg_years
unlock_query = "update reg_years set active = 'active' where id = " + str(year_id)
cursor.execute(unlock_query)
con.commit()

#%% test start here #############################################################

